<?php
  //get post data
  $arg_a = urldecode($_POST['a']);

  //demo address http://lowrez.net/apply/index.php
  $email_address = 'JesseRayZiegler@gmail.com';
  $my_name = 'Jesse Ziegler';
  $src_location = 'https://bitbucket.org/low-rez/storymix-application';
  $resume_location = 'http://lowrez.net/apply/jzieglerresume10.12.13.pdf';
    
  if ($_SERVER["REQUEST_METHOD"] == "POST")
  {
    if($arg_a == 'Ping')
      print('<div class="text">'.'OK'.'</div>');

    if($arg_a == 'Email Address')
      print('<div class="text"><a href="mailto:'.$email_address.'?Subject=STORYMIX">'.$email_address.'</a></div>');
      
    if($arg_a == 'Name')
      print('<div class="text">'.$my_name.'</div>');

    if($arg_a == 'Question')
    {
      $arg_b = $_POST['q'];
      $arr = explode(" ", $arg_b);
      $operand_a = $arr[2];
      $operand_b = $arr[4];

      $result = $operand_a * $operand_b;
      print('<div class="text">'.$result.'</div>');
    }

    if($arg_a == 'Source') 
      print('<div class="text"><a href="'.$src_location.'">'.$src_location.'</a></div>');

    if($arg_a == 'Resume')
      print('<div class="text"><a href="'.$resume_location.'">'.$resume_location.'</a></div>');
  }
?>